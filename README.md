# Steps to set up example

1. clone this repo and the sourceability frontend repo to your local machine
2. in the frontend repo, switch to `feature/FE-6-button-component` branch
3. run `yarn`, `yarn link` and `yarn frontend build` in the frontend repo (**order matters**)
4. run `yarn`, `yarn link @sourceability/frontend`, and `yarn build` in this repo (**order matters**)
5. Open `index.html` in browser to see integrated button component
