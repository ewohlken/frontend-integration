import Vue from 'vue';
import VueCompositionApi from '@vue/composition-api'
import {Button} from '@sourceability/frontend/dist/frontend.js';

console.log(Button);

Vue.use(VueCompositionApi)

var app = new Vue({ 
    el: '#app',
    data: {
      message: 'Hello Vue!'
    },
    components: {
      Button
    },
    template: `
        <div>
            <h1>{{ message }}</h1>
            <Button modifiers="secondary">test</Button>
        </div>
    `
  })