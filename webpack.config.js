const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');


module.exports = {
    mode: 'production',
    module: {
      rules: [
        {
          test: /\.js$/,
          include: require('path').resolve(__dirname, 'src'),
          use: {
            loader: "babel-loader"
          },
        },
        {
          test: /\.vue$/,
          use: [
              'vue-loader'
          ]
        }
      ]
    },
    resolve: { 
      alias: { 
        vue: path.resolve(__dirname,  'node_modules', 'vue/dist/vue.esm.js' ),
        '@vue/composition-api': path.resolve(__dirname,  'node_modules', '@vue/composition-api' )
      }
    },
  }